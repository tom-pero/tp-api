const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors')
const requireDir = require('require-dir');
const swagger = require('swagger-ui-express');
const swaggerDoc = require('./swagger.json')



//run
const app = express();
app.use(express.json());
app.use(cors())
app.use('/api-docs', swagger.serve, swagger.setup(swaggerDoc));

//connect in database
mongoose.connect('mongodb://admin:$Projeto00@ds217438.mlab.com:17438/heroku_mbcrzlph',{
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useCreateIndex: true
})

requireDir('./src/models/')

app.use('/', require('./src/routes'))
app.listen(process.env.PORT || 3000);