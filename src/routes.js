const express = require('express');

const routes = express.Router();
const RecipesController = require('./controllers/RecipeController')
const UserController = require('./controllers/UserController')

//RecipesController
routes.get('/recipes', RecipesController.index);
routes.post('/recipes', RecipesController.register);
routes.get('/recipes/:id', RecipesController.show);
routes.put('/recipes/:id', RecipesController.update);
routes.delete('/recipes/:id', RecipesController.remove);

//UserController
routes.get('/user/:id', UserController.index);
routes.post('/user', UserController.post);
routes.delete('/user/:id', UserController.remove);
routes.put('/user/:id', UserController.update);

module.exports = routes