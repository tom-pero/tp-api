const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate')

const RecipeSchema = new mongoose.Schema({
    recipe_title: {
        type: String,
        required: true
    },
    
    ingredients: {
        type: String,
        required: true,
    },

    preparation: {
        type: String,
        required: true,
      	
    },

    hint: {
        type: String,
        required: false
    },

    rating_id: [
        {
            type: mongoose.SchemaTypes.ObjectId, ref: 'Recipe',
            default: null
        }
    ],

    creation_date: {
        type: Date,
        default: Date.now,
    },

    difficulty_id:{
        type : Number,
        required: true
    },

    photos: {
        type: String,
        required: false,
    },

});
RecipeSchema.plugin(mongoosePaginate)
const Recipe = mongoose.model('Recipe', RecipeSchema);
module.exports = Recipe;