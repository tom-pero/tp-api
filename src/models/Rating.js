const mongoose = require('mongoose')

const RatingSchema = new mongoose.Schema({
    recipe_id:
        {type: mongoose.SchemaTypes.ObjectId, ref: 'Recipe'}
    ,
    //quantidade de avaliacoes
    amount:{
        type: Object,
        require : false
    },
    //media das avaliações
    mean:{
        type: Number,
        require : false
    },
    
    comment:{
        type: String,
        required: true
    }
})

const Rating = mongoose.model('Rating', RatingSchema)
module.exports = Rating