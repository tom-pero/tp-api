const DIFFICULTY = {
    EASY: 1,
    INTERMEDIARY: 2,
    ADVANCED: 3
}

module.exports = DIFFICULTY
