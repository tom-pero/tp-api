const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },

    password: {
        type: String,
        required: true,
        select: false,
        length: {
                minimum: 5,
                maximum: 20
                }
    },

    certificate: {
        type: String,
        required: false
    },

    photo:{
        type : String,
        required: false
    }

});

const User = mongoose.model('User', UserSchema);
module.exports = User;