const mongoose = require('mongoose')

const User =  mongoose.model("User")

module.exports = {
    //GET
    async index(req, res){
        const user = await User.findById(req.params.id)
        return res.json(user)
    },
    //POST
    async post(req, res){
        const user = await User.create(req.body);
        return res.json(user)
    },
    //UPDATE
    async update(req, res){
        const user = await User.findByIdAndUpdate(req.params.id, req.body, {new: true})
        return res.json(user)
    },
    //DELETE
    async remove(req, res){
        const user = await User.findByIdAndRemove()
        return res.send("Delete Sucessful")
    }
}