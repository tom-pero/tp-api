const mongoose = require('mongoose')

const Recipe = mongoose.model("Recipe");
const Rating = mongoose.model("Rating");

module.exports = {
    //GET
    async index(req, res){
        const { page } = req.query;
        const { difficulty } = req.query;
        const { rating } = req.query;

        if(rating > 0){
            //get id recipe from ratings > 3
            const rating = await Rating.find({mean : {$gt: 3}}).select("recipe_id -_id")
            console.log(rating)
            //convert
            const numbers = rating.map(v  => v.recipe_id)
            console.log(numbers)   
            const recipe = await Recipe.paginate({_id: {$in: (numbers)} }, {page, limit: 4})

            return res.json(recipe)
        }
        else if(difficulty > 0){
            const recipe = await Recipe.paginate({difficulty_id :{$gte : 4}},{page, limit: 10})
            return res.json(recipe)
        }else{
            const recipe = await Recipe.paginate({}, {page, limit: 4});
            return res.json(recipe);
        }
    },
    //POST
    async register(req, res){
        const recipe = await Recipe.create(req.body);
        return res.json(recipe);
    },
    //GET
    async show(req, res){
        const recipe = await Recipe.findById(req.params.id);
        return res.json(recipe)
    },
    //UPDATE
    async update(req, res){
        if (Object.keys(req.query).length === 0) {
            const recipe = await Recipe.findByIdAndUpdate(req.params.id, req.body, {new: true});
            return res.json(recipe)
        }else{ 
            try{
                //get all ratings from that recipe 
                const queryAmounts = Rating.find({recipe_id: req.params.id}).select('amount -_id')
                const obj = await queryAmounts.exec()
                //add a new rating
                obj.push({amount: req.query.rating})
                //convert to array
                const numbers = obj.map(v  => v.amount)
                //sum all items
                const sum = numbers.reduce((a, b) => parseInt(a) + parseInt(b), 0)
                const rating = await Rating.create({
                    recipe_id : req.params.id,
                    amount : parseInt(req.query.rating),
                    mean: sum / obj.length,
                    comment : req.body.comment
                });
                const recipe = await Recipe.findByIdAndUpdate(req.params.id, {rating_id: rating._id}, {new : true})
                return res.json(rating)

            }catch(error){
                res.status(500).send("Internal Server Error")
            }
        }
    },
    //DELETE
    async remove(req, res){
        const recipe = await Recipe.findByIdAndRemove(req.params.id)
        return res.send("Recipe Removed");
    }
}

function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }